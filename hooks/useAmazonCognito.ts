import cognitoUser from "@config/cognitoUser";
import { LOCALSTORAGE_KEY } from "@config/constants";
import authStore from "@store/authStore";
import mfaStore, { MfaStoreTypes } from "@store/mfaStore";
import userPool from "@userPool";
import {
  AuthenticationDetails,
  CognitoAccessToken,
  CognitoIdToken,
  CognitoRefreshToken,
  CognitoUser,
  CognitoUserAttribute,
  CognitoUserPool,
  CognitoUserSession,
  CookieStorage,
} from "amazon-cognito-identity-js";
import { useStore } from "zustand";
import useLocalStorage from "./useLocalStorage";
import { shallow } from "zustand/shallow";
import { useStoreWithEqualityFn } from "zustand/traditional";

const useAmazonCognito = () => {
  const { setItem, removeItem, getItem } = useLocalStorage();
  const SecurityStore = useStoreWithEqualityFn(
    authStore,
    (state) => state,
    shallow
  );
  const MfaStore = useStoreWithEqualityFn(
    mfaStore,
    (state) => state,
    shallow
  ) as MfaStoreTypes;

  const { data: userData, setData } = SecurityStore;
  const { data: mfaData, setData: setMfaData } = MfaStore || {};

  const register = (values: any) => {
    const { username, password, email } = values || {};
    return new Promise((res, rej) => {
      var attributeList = [];
      var dataEmail = {
        Name: "email",
        Value: email,
      };

      var attributeEmail = new CognitoUserAttribute(dataEmail);
      attributeList.push(attributeEmail);

      userPool.signUp(username, password, attributeList, [], (err, data) => {
        if (!err) {
          res(data);
        } else {
          rej(err);
        }
      });
    });
  };

  const verify = (values: { username: string; otp: string }) => {
    const { otp, username } = values;
    return new Promise((res, rej) => {
      cognitoUser(username).confirmRegistration(otp, true, (err, result) => {
        if (err) {
          rej(err);
        } else {
          res(result);
        }
      });
    });
  };

  const resendOtp = (values: { username: string }) => {
    const { username } = values;
    return new Promise((res, rej) => {
      cognitoUser(String(username)).resendConfirmationCode((err, result) => {
        if (err) {
          rej(err);
        } else {
          res(result);
        }
      });
    });
  };

  const forgotPassword = (values: { username: string }) => {
    const { username } = values;
    return new Promise((res, rej) => {
      cognitoUser(String(username)).forgotPassword({
        onSuccess: (result) => {
          res(result);
        },
        onFailure: (err) => {
          rej(err);
        },
      });
    });
  };

  const confirmPassword = (values: {
    username: string;
    otp: string;
    new_password: string;
  }) => {
    const { username, new_password: newPassword, otp } = values;
    return new Promise((res, rej) => {
      cognitoUser(String(username)).confirmPassword(otp, newPassword, {
        onSuccess: (result) => {
          res(result);
        },
        onFailure: (err) => {
          rej(err);
        },
      });
    });
  };

  const login = (email: string, password: string) => {
    return new Promise((res, rej) => {
      const user = cognitoUser(email);
      const authDetails = new AuthenticationDetails({
        Username: email,
        Password: password,
      });
      user.authenticateUser(authDetails, {
        mfaSetup: () => {
          user.associateSoftwareToken({
            associateSecretCode: async (secretCode) => {
              const uri = `otpauth://totp/${decodeURI(
                email
              )}?secret=${secretCode}`;

              res({
                uri,
                user,
              });
            },
            onFailure: (err) => {
              rej(err);
            },
          });
        },
        totpRequired: () => {
          res({ user });
        },
        onSuccess: async (data: any) => {
          user.associateSoftwareToken({
            associateSecretCode: async (secretCode) => {
              const uri = `otpauth://totp/${data?.idToken?.payload?.email}?secret=${secretCode}`;
              res({ uri, user });
            },
            onFailure: (err) => {
              rej(err);
            },
          });
        },
        onFailure: (err) => {
          rej(err);
        },
      });
    });
  };

  const mfaLoggedHandler = () => {
    return new Promise((res, rej) => {
      mfaData?.cognitoUser?.getSession((err: Error, data: any) => {
        if (err) {
          rej(err);
        } else {
          const unix = data.accessToken.payload.exp;

          setItem(LOCALSTORAGE_KEY.ACCESS_TOKEN, data.accessToken.jwtToken);
          setItem(LOCALSTORAGE_KEY.ID_TOKEN, data.idToken.jwtToken);
          setItem(LOCALSTORAGE_KEY.REFRESH_TOKEN, data.refreshToken.token);
          setItem(LOCALSTORAGE_KEY.ACCOUNT, data.accessToken.payload.username);
          setItem(LOCALSTORAGE_KEY.EXPIRED_TIME, unix);

          setData(data);

          res(data);
        }
      });
    });
  };

  const verifyTotp = (totp: string) => {
    return new Promise((res, rej) => {
      mfaData?.cognitoUser?.verifySoftwareToken(totp, "My TOTP Device", {
        onSuccess: (session) => {
          mfaData?.cognitoUser?.setUserMfaPreference(
            null,
            {
              Enabled: true,
              PreferredMfa: true,
            },
            (error, success) => {
              if (error) {
                rej(error);
              } else {
                mfaLoggedHandler()
                  .then(() => {
                    res(session);
                  })
                  .catch((err) => {
                    rej(err);
                  });
              }
            }
          );
        },
        onFailure: (err) => {
          rej(err);
        },
      });
    });
  };

  const sendMfaCode = (code: string) => {
    return new Promise((res, rej) => {
      mfaData?.cognitoUser?.sendMFACode(
        code,
        {
          onSuccess: (session) => {
            mfaLoggedHandler()
              .then(() => {
                res(session);
              })
              .catch((err) => {
                rej(err);
              });
          },
          onFailure: (err) => {
            rej(err);
          },
        },
        "SOFTWARE_TOKEN_MFA"
      );
    });
  };

  const resetMfa = () => {
    return new Promise((res, rej) => {
      cognitoUser()?.setUserMfaPreference(
        null,
        {
          Enabled: false,
          PreferredMfa: false,
        },
        (error, success) => {
          if (error) {
            rej(error);
          } else {
            res(success);
          }
        }
      );
    });
  };

  const getSession = async () => {
    return await new Promise((res, reject) => {
      const user = userPool.getCurrentUser();

      if (user) {
        user?.getSession((err: any, session: any) => {
          if (err) {
            reject();
          } else {
            res(session);
          }
        });
      } else {
        reject();
      }
    });
  };

  const refreshToken = async () => {
    return new Promise((res, rej) => {
      cognitoUser().getSession(
        (err: Error, session: CognitoUserSession | any) => {
          const refresh = new CognitoRefreshToken({
            RefreshToken: session.getRefreshToken().getToken(),
          });
          if (err || !session.getRefreshToken().getToken()) {
            rej(err);
          } else {
            if (session.isValid()) {
              res("");
            } else {
              cognitoUser().refreshSession(
                refresh,
                (errRefresh, sessionRefresh) => {
                  if (errRefresh) {
                    logout();
                  } else {
                    setData(sessionRefresh);
                    res(sessionRefresh);
                  }
                }
              );
            }
          }
        }
      );
    });
  };

  const logout = () => {
    const user = userPool.getCurrentUser();

    if (user) {
      user.signOut();
    }
    removeItem(LOCALSTORAGE_KEY.ACCESS_TOKEN);
    removeItem(LOCALSTORAGE_KEY.REFRESH_TOKEN);
    removeItem(LOCALSTORAGE_KEY.EXPIRED_TIME);
    removeItem(LOCALSTORAGE_KEY.ACCOUNT);
    removeItem(LOCALSTORAGE_KEY.ID_TOKEN);

    setData({ data: {} });
    setMfaData({
      cognitoUser: null,
      totpToken: "",
      username: "",
    });

    if (!window.location.href.includes("login")) {
      window.location.href = "/login";
    }
  };

  return {
    register,
    verify,
    resendOtp,
    forgotPassword,
    confirmPassword,
    login,
    sendMfaCode,
    resetMfa,
    verifyTotp,
    getSession,
    refreshToken,
    logout,
  };
};

export default useAmazonCognito;
