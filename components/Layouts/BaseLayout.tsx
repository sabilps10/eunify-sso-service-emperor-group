import { FC, PropsWithChildren, useEffect, useState } from "react";

import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import { useRouter } from "next/router";
import useMounted from "@hooks/useMounted";
import { Toaster } from "react-hot-toast";
import { Button, Typography } from "@mui/material";
import authStore, { AuthStoreTypes } from "@store/authStore";
import { shallow } from "zustand/shallow";
import useLocalStorage from "@hooks/useLocalStorage";
import {
  LOCALSTORAGE_KEY,
  NON_LOGIN_ROUTES_KEY,
  ROUTES_KEY,
} from "@config/constants";
import useAmazonCognito from "@hooks/useAmazonCognito";
import cognitoUser from "@config/cognitoUser";
import { CognitoUserSession } from "amazon-cognito-identity-js";

const BaseLayout: FC<PropsWithChildren> = ({ children }) => {
  const { pathname, ...props } = useRouter();
  const [loading, setLoading] = useState(false);

  const SecurityStore = authStore((state) => state, shallow) as AuthStoreTypes;
  const { data } = SecurityStore;

  const { getItem } = useLocalStorage();
  const { logout, refreshToken } = useAmazonCognito();

  const router = useRouter();
  const {
    query: { feature },
  } = router || {};

  useEffect(() => {
    getUser();
  }, []);

  const getUser = () => {
    refreshToken()
      .then(() => {
        if (NON_LOGIN_ROUTES_KEY.includes(String(feature))) {
          router.replace("/home");
        }
      })
      .catch((err) => {
        if (ROUTES_KEY.includes(String(feature)) || feature === "") {
          router.replace("/login");
        }
      })
      .finally(() => {
        setLoading(true);
      });
  };

  if (!loading) return <></>;
  return (
    <Box sx={{ display: "flex" }}>
      <Toaster />
      <Box
        component="main"
        sx={{
          backgroundColor: (theme) => theme.palette.grey[100],
          flexGrow: 1,
          height: "100vh",
          overflow: "auto",
        }}
      >
        <Toolbar
          sx={(theme) => ({
            background: theme.palette.brandYellow[500],
            pr: "24px",
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
          })}
        >
          <Typography color={(theme) => theme.palette.neutrals.white}>
            Amazon Cognito Test
          </Typography>
          {data?.idToken && (
            <Button variant="text" onClick={() => logout()}>
              <Typography color={(theme) => theme.palette.neutrals.white}>
                LOGOUT
              </Typography>
            </Button>
          )}
        </Toolbar>
        <Container maxWidth={false}>
          <Grid container>{children}</Grid>
        </Container>
      </Box>
    </Box>
  );
};

export default BaseLayout;
