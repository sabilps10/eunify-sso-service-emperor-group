import cognitoUser from "@config/cognitoUser";
import useAmazonCognito from "@hooks/useAmazonCognito";
import {
  Box,
  Button,
  CircularProgress,
  Container,
  Stack,
  Typography,
} from "@mui/material";
import authStore, { AuthStoreTypes } from "@store/authStore";
import React, { useEffect, useState } from "react";
import toast from "react-hot-toast";
import { shallow } from "zustand/shallow";

const Home = () => {
  const SecurityStore = authStore((state) => state, shallow) as AuthStoreTypes;
  const { data } = SecurityStore;

  const [loading, setLoading] = useState(false);

  const { resetMfa, logout } = useAmazonCognito();

  const resetHandler = () => {
    setLoading(true);
    resetMfa()
      .then((res) => {
        toast("Existing MFA has been removed, please login again");
        logout();
      })
      .catch((err: any) => {
        toast(err?.message || "");
      })
      .finally(() => setLoading(false));
  };

  return (
    <Container component={"main"} maxWidth="xs">
      <Box
        sx={{
          background: "#FFF",
          marginTop: 8,
          display: "flex",
          flexDirection: "column",
          alignItems: "stretch",
          borderRadius: 3,
          padding: 3,
          boxShadow: 1,
        }}
      >
        <Stack width={"100%"} gap={2}>
          <Typography>
            Your username:{" "}
            <Typography fontWeight={700}>
              {data?.idToken?.payload?.["cognito:username"]}
            </Typography>
          </Typography>
          <Typography>
            Your email:{" "}
            <Typography fontWeight={700}>
              {data?.idToken?.payload?.email}
            </Typography>
          </Typography>
          <Typography>
            Email verified:{" "}
            <Typography
              fontWeight={700}
              color={(theme) =>
                !data?.idToken?.payload?.email_verified
                  ? theme.palette.brandRed[500]
                  : theme.palette.brandBlue[800]
              }
            >
              {String(data?.idToken?.payload?.email_verified)}
            </Typography>
          </Typography>

          <Button sx={{ mt: 3 }} onClick={resetHandler} disabled={loading}>
            {loading ? (
              <CircularProgress size={22} />
            ) : (
              <Typography>Remove Existing MFA</Typography>
            )}
          </Button>
        </Stack>
      </Box>
    </Container>
  );
};

export default Home;
