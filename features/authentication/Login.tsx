import { Container } from "@mui/material";
import mfaStore, { MfaStoreTypes } from "@store/mfaStore";
import Head from "next/head";
import { shallow } from "zustand/shallow";
import LoginForm from "./fragments/LoginForm";
import LoginMfaForm from "./fragments/LoginMFAForm";

const Login = () => {
  const MfaStore = mfaStore((state) => state, shallow) as MfaStoreTypes;
  const { data } = MfaStore;

  return (
    <>
      <Head>
        <title>Login</title>
      </Head>
      <Container component={"main"} maxWidth="xs">
        {data?.cognitoUser ? <LoginMfaForm /> : <LoginForm />}
      </Container>
    </>
  );
};

export default Login;
