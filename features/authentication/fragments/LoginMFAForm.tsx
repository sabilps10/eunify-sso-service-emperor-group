import InputText from "@components/FormFields/InputText";
import { mfaSchema } from "@config/form-fields/auth";
import { zodResolver } from "@hookform/resolvers/zod";
import useAmazonCognito from "@hooks/useAmazonCognito";
import {
  Alert,
  Box,
  Button,
  CircularProgress,
  Stack,
  Typography,
} from "@mui/material";
import mfaStore, { MfaStoreTypes } from "@store/mfaStore";
import { useRouter } from "next/router";
import { FC, useState } from "react";
import { FormProvider, useForm } from "react-hook-form";
import QRCode from "react-qr-code";
import { TypeOf } from "zod";
import { shallow } from "zustand/shallow";

type Props = {};

type LoginMfaInput = TypeOf<typeof mfaSchema>;
const LoginMfaForm: FC<Props> = ({}) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<ErrorCognito | null>(null);

  const methods = useForm<LoginMfaInput>({
    resolver: zodResolver(mfaSchema),
    defaultValues: {
      totp: "",
    },
  });
  const { handleSubmit, setValue, watch } = methods;

  const MfaStore = mfaStore((state) => state, shallow) as MfaStoreTypes;
  const { setData, data } = MfaStore;

  const { verifyTotp, sendMfaCode } = useAmazonCognito();
  const router = useRouter();

  const onSubmitHandler = (values: any) => {
    setLoading(true);
    if (data.totpToken) {
      verifyTotp(values?.totp)
        .then((res: any) => {
          setData({
            ...data,
            totpToken: "",
            username: "",
          });
          router.push("/home");
        })
        .catch((err) => {
          setError(err);
        })
        .finally(() => {
          setLoading(false);
        });
    } else {
      sendMfaCode(values?.totp)
        .then((res: any) => {
          setData({
            ...data,
            totpToken: "",
            username: "",
          });
          router.push("/home");
        })
        .catch((err) => {
          setError(err);
        })
        .finally(() => {
          setLoading(false);
        });
    }
  };

  return (
    <FormProvider {...methods}>
      <Box
        component="form"
        noValidate
        autoComplete="off"
        autoCorrect="off"
        onSubmit={handleSubmit(onSubmitHandler)}
        sx={{
          background: "#FFF",
          marginTop: 8,
          display: "flex",
          flexDirection: "column",
          alignItems: "stretch",
          borderRadius: 3,
          padding: 3,
          boxShadow: 1,
        }}
      >
        <Stack width={"100%"}>
          <Typography
            variant="button"
            marginBottom={2}
            color={(theme) => theme.palette.brandYellow[200]}
            onClick={() =>
              setData({
                cognitoUser: null,
                totpToken: "",
                username: "",
              })
            }
          >
            {"< Back"}
          </Typography>
          <Typography variant="h2" marginBottom={4}>
            Multi Factor Authentication
          </Typography>
          {error && (
            <Alert severity="error">
              <Typography>{error?.message}</Typography>
            </Alert>
          )}
          <Stack gap={2} mt={error ? 2 : 0}>
            {data?.totpToken && (
              <Stack display={"flex"} alignItems={"center"}>
                <QRCode value={data?.totpToken} />
              </Stack>
            )}
            <InputText
              disabled={loading}
              name="totp"
              required={true}
              fullWidth={true}
              label={"TOTP Code"}
              set={setValue}
            />
          </Stack>
          <Button type="submit" fullWidth sx={{ mt: 3 }} disabled={loading}>
            {loading ? (
              <CircularProgress size={22} />
            ) : (
              <Typography variant="body" fontWeight={700}>
                Submit
              </Typography>
            )}
          </Button>
        </Stack>
      </Box>
    </FormProvider>
  );
};

export default LoginMfaForm;
