import InputText from "@components/FormFields/InputText";
import { changePasswordSchema } from "@config/form-fields/auth";
import { zodResolver } from "@hookform/resolvers/zod";
import useAmazonCognito from "@hooks/useAmazonCognito";
import {
  Alert,
  Box,
  Button,
  CircularProgress,
  Stack,
  Typography,
} from "@mui/material";
import { useRouter } from "next/router";
import { FC, useState } from "react";
import { FormProvider, useForm } from "react-hook-form";
import toast from "react-hot-toast";
import { TypeOf } from "zod";

type Props = {
  firstData: {
    username: string;
    securedEmail: string;
  };
};

type ChangePasswordInput = TypeOf<typeof changePasswordSchema>;
const ChangePasswordForm: FC<Props> = ({ firstData }) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [errorMessage, setErrorMessage] = useState<string | null>(null);

  const methods = useForm<ChangePasswordInput>({
    resolver: zodResolver(changePasswordSchema),
    defaultValues: {
      otp: "",
      new_password: "",
    },
  });
  const { handleSubmit, setValue, watch } = methods;

  const { confirmPassword } = useAmazonCognito();
  const router = useRouter();

  const onSubmitHandler = (values: any) => {
    setLoading(true);
    confirmPassword({
      username: firstData?.username,
      new_password: watch("new_password"),
      otp: watch("otp"),
    })
      .then(() => {
        router.replace("/login");
        toast("Your password has been changed");
      })
      .catch((err) => {
        setErrorMessage(err?.message);
      })
      .finally(() => {
        setLoading(false);
      });
  };
  return (
    <FormProvider {...methods}>
      <Box
        component="form"
        noValidate
        autoComplete="off"
        autoCorrect="off"
        onSubmit={handleSubmit(onSubmitHandler)}
        sx={{
          background: "#FFF",
          marginTop: 8,
          display: "flex",
          flexDirection: "column",
          alignItems: "stretch",
          borderRadius: 3,
          padding: 3,
          boxShadow: 1,
        }}
      >
        <Stack width={"100%"}>
          <Typography variant="h2" marginBottom={4}>
            Change Password
          </Typography>
          {errorMessage && (
            <Stack mb={2}>
              <Alert severity="error">{errorMessage}</Alert>
            </Stack>
          )}

          <Alert severity="success">{`Your otp code has been sent to ${firstData?.securedEmail}`}</Alert>

          <Stack gap={2} mt={2}>
            <InputText
              disabled={loading}
              name="otp"
              required={true}
              fullWidth={true}
              label={"OTP"}
              set={setValue}
            />
            <InputText
              disabled={loading}
              name="new_password"
              required={true}
              fullWidth={true}
              label={"New password"}
              set={setValue}
            />
          </Stack>
          <Button type="submit" fullWidth sx={{ mt: 3 }} disabled={loading}>
            {loading ? (
              <CircularProgress size={22} />
            ) : (
              <Typography variant="body" fontWeight={700}>
                Submit
              </Typography>
            )}
          </Button>
        </Stack>
      </Box>
    </FormProvider>
  );
};

export default ChangePasswordForm;
