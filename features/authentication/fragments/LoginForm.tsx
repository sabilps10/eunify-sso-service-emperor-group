import InputText from "@components/FormFields/InputText";
import cognitoUser from "@config/cognitoUser";
import { AUTH_EXCEPTION_CODE } from "@config/errors";
import { loginSchema } from "@config/form-fields/auth";
import { zodResolver } from "@hookform/resolvers/zod";
import useAmazonCognito from "@hooks/useAmazonCognito";
import {
  Alert,
  Box,
  Button,
  CircularProgress,
  Container,
  Stack,
  Typography,
} from "@mui/material";
import mfaStore, { MfaStoreTypes } from "@store/mfaStore";
import Head from "next/head";
import { useRouter } from "next/router";
import { FC, useState } from "react";
import { FormProvider, useForm } from "react-hook-form";
import { TypeOf } from "zod";
import { shallow } from "zustand/shallow";

type Props = {};

type LoginInput = TypeOf<typeof loginSchema>;
const LoginForm: FC<Props> = ({}) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<ErrorCognito | null>(null);

  const MfaStore = mfaStore((state) => state, shallow) as MfaStoreTypes;
  const { setData, data } = MfaStore;

  const methods = useForm<LoginInput>({
    resolver: zodResolver(loginSchema),
    defaultValues: {
      username: "",
      password: "",
    },
  });
  const { handleSubmit, setValue, watch } = methods;

  const { login, resendOtp } = useAmazonCognito();
  const router = useRouter();

  const onSubmitHandler = (values: any) => {
    setLoading(true);
    login(values.username, values.password)
      .then((res: any) => {
        const { uri, user } = res || {};

        if (uri) {
          setData({
            cognitoUser: user,
            totpToken: uri,
            username: values?.username,
          });
        } else {
          setData({
            cognitoUser: user,
            username: values?.username,
            totpToken: "",
          });
        }
      })
      .catch((err) => {
        setError(err);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const onConfirm = () => {
    resendOtp({ username: watch("username") })
      .then(() => {
        router.push(`/verify?username=${watch("username")}`);
      })
      .catch((err: ErrorCognito) => {
        setError(err);
      })
      .finally(() => setLoading(false));
  };
  return (
    <FormProvider {...methods}>
      <Box
        component="form"
        noValidate
        autoComplete="off"
        autoCorrect="off"
        onSubmit={handleSubmit(onSubmitHandler)}
        sx={{
          background: "#FFF",
          marginTop: 8,
          display: "flex",
          flexDirection: "column",
          alignItems: "stretch",
          borderRadius: 3,
          padding: 3,
          boxShadow: 1,
        }}
      >
        <Stack width={"100%"}>
          <Typography variant="h2" marginBottom={4}>
            Login
          </Typography>
          {error && (
            <Alert severity="error">
              <Typography>{error?.message}</Typography>
              {error?.code === AUTH_EXCEPTION_CODE.USER_NOT_CONFIRMED && (
                <Typography
                  color={(theme) => theme.palette.brandYellow[200]}
                  sx={{
                    ":hover": {
                      cursor: "pointer",
                    },
                  }}
                  onClick={onConfirm}
                >
                  Confirm here
                </Typography>
              )}
            </Alert>
          )}
          <Stack gap={2} mt={error ? 2 : 0}>
            <InputText
              disabled={loading}
              name="username"
              required={true}
              fullWidth={true}
              label={"Username"}
              set={setValue}
            />
            <InputText
              disabled={loading}
              name="password"
              required={true}
              fullWidth={true}
              label={"Password"}
              additionalRegex={false}
              set={setValue}
            />
          </Stack>
          <Button type="submit" fullWidth sx={{ mt: 3 }} disabled={loading}>
            {loading ? (
              <CircularProgress size={22} />
            ) : (
              <Typography variant="body" fontWeight={700}>
                Login
              </Typography>
            )}
          </Button>
          <Stack mt={2}>
            <Button onClick={() => router.push("/register")} variant="text">
              Register
            </Button>
          </Stack>
          <Stack
            direction={"row"}
            alignItems={"center"}
            justifyContent={"center"}
            mt={4}
          >
            <Typography variant="button">{"Forgot your password? "}</Typography>
            <Button
              onClick={() => router.push("/forgot-password")}
              variant="text"
            >
              here
            </Button>
          </Stack>
        </Stack>
      </Box>
    </FormProvider>
  );
};

export default LoginForm;
