import InputText from "@components/FormFields/InputText";
import cognitoUser from "@config/cognitoUser";
import { forgotPasswordSchema, loginSchema } from "@config/form-fields/auth";
import { zodResolver } from "@hookform/resolvers/zod";
import useAmazonCognito from "@hooks/useAmazonCognito";
import {
  Alert,
  Box,
  Button,
  CircularProgress,
  Container,
  Stack,
  Typography,
} from "@mui/material";
import Head from "next/head";
import { useRouter } from "next/router";
import { FC, useState } from "react";
import { FormProvider, useForm } from "react-hook-form";
import { TypeOf } from "zod";

type Props = {
  onSuccess: (username: string, securedEmail: string) => void;
};

type ForgotPasswordInput = TypeOf<typeof forgotPasswordSchema>;
const ForgotPasswordForm: FC<Props> = ({ onSuccess }) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [errorMessage, setErrorMessage] = useState<string | null>(null);

  const methods = useForm<ForgotPasswordInput>({
    resolver: zodResolver(forgotPasswordSchema),
    defaultValues: {
      username: "",
    },
  });
  const { reset, handleSubmit, setValue } = methods;

  const { forgotPassword, confirmPassword } = useAmazonCognito();
  const router = useRouter();

  const onSubmitHandler = (values: any) => {
    setLoading(true);
    forgotPassword({ username: values?.username })
      .then((res: any) => {
        onSuccess &&
          onSuccess(values?.username, res?.CodeDeliveryDetails?.Destination);
      })
      .catch((err) => {
        setErrorMessage(err?.message);
      })
      .finally(() => {
        setLoading(false);
      });
  };
  return (
    <FormProvider {...methods}>
      <Box
        component="form"
        noValidate
        autoComplete="off"
        autoCorrect="off"
        onSubmit={handleSubmit(onSubmitHandler)}
        sx={{
          background: "#FFF",
          marginTop: 8,
          display: "flex",
          flexDirection: "column",
          alignItems: "stretch",
          borderRadius: 3,
          padding: 3,
          boxShadow: 1,
        }}
      >
        <Stack width={"100%"}>
          <Typography variant="h2" marginBottom={4}>
            Forgot Password
          </Typography>
          {errorMessage && <Alert severity="error">{errorMessage}</Alert>}

          <Stack gap={2} mt={errorMessage ? 2 : 0}>
            <InputText
              disabled={loading}
              name="username"
              required={true}
              fullWidth={true}
              label={"Username"}
              set={setValue}
            />
          </Stack>
          <Button type="submit" fullWidth sx={{ mt: 3 }} disabled={loading}>
            {loading ? (
              <CircularProgress size={22} />
            ) : (
              <Typography variant="body" fontWeight={700}>
                Submit
              </Typography>
            )}
          </Button>
          <Stack
            direction={"row"}
            alignItems={"center"}
            justifyContent={"center"}
            mt={4}
          >
            <Typography variant="button">
              {"Remember your password? "}
            </Typography>
            <Button onClick={() => router.push("/login")} variant="text">
              Login
            </Button>
          </Stack>
        </Stack>
      </Box>
    </FormProvider>
  );
};

export default ForgotPasswordForm;
