import InputText from "@components/FormFields/InputText";
import cognitoUser from "@config/cognitoUser";
import {
  loginSchema,
  registerSchema,
  verifySchema,
} from "@config/form-fields/auth";
import { zodResolver } from "@hookform/resolvers/zod";
import useAmazonCognito from "@hooks/useAmazonCognito";
import {
  Alert,
  Box,
  Button,
  CircularProgress,
  Container,
  Stack,
  Typography,
} from "@mui/material";
import Head from "next/head";
import { useRouter } from "next/router";
import { useState } from "react";
import { FormProvider, useForm } from "react-hook-form";
import toast from "react-hot-toast";
import { TypeOf } from "zod";

type VerifyInput = TypeOf<typeof verifySchema>;
const Verify = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [errorMessage, setErrorMessage] = useState<string | null>(null);
  const [successMessage, setSuccessMessage] = useState<string | null>(null);

  const methods = useForm<VerifyInput>({
    resolver: zodResolver(verifySchema),
    defaultValues: {
      otp: "",
    },
  });
  const { reset, handleSubmit, setValue } = methods;

  const { verify, resendOtp } = useAmazonCognito();
  const router = useRouter();
  const { username } = router.query || {};

  const onSubmitHandler = (values: any) => {
    setLoading(true);
    verify({ username: String(username), otp: values?.otp })
      .then(() => {
        toast("Verify success, please login");
        router.replace("/login");
      })
      .catch((err) => {
        setErrorMessage(err?.message);
        setSuccessMessage(null);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const handleResend = () => {
    setLoading(true);
    resendOtp({ username: String(username) })
      .then(() => {
        setSuccessMessage("Sent");
      })
      .catch((err: any) => {
        setErrorMessage(err?.message);
      })
      .finally(() => setLoading(false));
  };

  return (
    <>
      <Head>
        <title>Verify</title>
      </Head>
      <Container component={"main"} maxWidth="xs">
        <FormProvider {...methods}>
          <Box
            component="form"
            noValidate
            autoComplete="off"
            autoCorrect="off"
            onSubmit={handleSubmit(onSubmitHandler)}
            sx={{
              background: "#FFF",
              marginTop: 8,
              display: "flex",
              flexDirection: "column",
              alignItems: "stretch",
              borderRadius: 3,
              padding: 3,
              boxShadow: 1,
            }}
          >
            <Stack width={"100%"}>
              <Typography variant="h2" marginBottom={4}>
                Verify
              </Typography>
              {errorMessage && <Alert severity="error">{errorMessage} </Alert>}
              {successMessage && (
                <Alert severity="success">{successMessage}</Alert>
              )}
              <Stack gap={2} mt={errorMessage || successMessage ? 2 : 0}>
                <InputText
                  disabled={loading}
                  name="otp"
                  required={true}
                  fullWidth={true}
                  label={"OTP"}
                  set={setValue}
                />
              </Stack>
              <Button type="submit" fullWidth sx={{ mt: 3 }} disabled={loading}>
                {loading ? (
                  <CircularProgress size={22} />
                ) : (
                  <Typography variant="body" fontWeight={700}>
                    Verify
                  </Typography>
                )}
              </Button>
              <Stack
                direction={"row"}
                alignItems={"center"}
                justifyContent={"center"}
                mt={4}
              >
                <Typography variant="button">
                  {"Not received the otp code? "}
                </Typography>
                <Button onClick={handleResend} variant="text">
                  Resend
                </Button>
              </Stack>
            </Stack>
          </Box>
        </FormProvider>
      </Container>
    </>
  );
};

export default Verify;
