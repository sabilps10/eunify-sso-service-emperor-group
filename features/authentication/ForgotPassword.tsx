import InputText from "@components/FormFields/InputText";
import cognitoUser from "@config/cognitoUser";
import { forgotPasswordSchema, loginSchema } from "@config/form-fields/auth";
import { zodResolver } from "@hookform/resolvers/zod";
import useAmazonCognito from "@hooks/useAmazonCognito";
import {
  Alert,
  Box,
  Button,
  CircularProgress,
  Container,
  Stack,
  Typography,
} from "@mui/material";
import Head from "next/head";
import { useRouter } from "next/router";
import { useState } from "react";
import { FormProvider, useForm } from "react-hook-form";
import { TypeOf } from "zod";
import ForgotPasswordForm from "./fragments/ForgotPasswordForm";
import ChangePasswordForm from "./fragments/ChangePasswordForm";

const ForgotPassword = () => {
  const [step, setStep] = useState<"first" | "second">("first");
  const [firstData, setFirstData] = useState<any>({});

  return (
    <>
      <Head>
        <title>Login</title>
      </Head>
      <Container component={"main"} maxWidth="xs">
        {step === "first" ? (
          <ForgotPasswordForm
            onSuccess={(username, securedEmail) => {
              setFirstData({
                username,
                securedEmail,
              });
              setStep("second");
            }}
          />
        ) : (
          <ChangePasswordForm firstData={firstData} />
        )}
      </Container>
    </>
  );
};

export default ForgotPassword;
