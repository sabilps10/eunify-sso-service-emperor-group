import InputText from "@components/FormFields/InputText";
import cognitoUser from "@config/cognitoUser";
import { loginSchema, registerSchema } from "@config/form-fields/auth";
import { zodResolver } from "@hookform/resolvers/zod";
import useAmazonCognito from "@hooks/useAmazonCognito";
import {
  Alert,
  Box,
  Button,
  CircularProgress,
  Container,
  Stack,
  Typography,
} from "@mui/material";
import Head from "next/head";
import { useRouter } from "next/router";
import { useState } from "react";
import { FormProvider, useForm } from "react-hook-form";
import { TypeOf } from "zod";

type RegisterInput = TypeOf<typeof registerSchema>;
const Register = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [errorMessage, setErrorMessage] = useState<string | null>(null);

  const methods = useForm<RegisterInput>({
    resolver: zodResolver(registerSchema),
    defaultValues: {
      username: "",
      email: "",
      password: "",
    },
  });
  const { reset, handleSubmit, setValue } = methods;

  const { register } = useAmazonCognito();
  const router = useRouter();

  const onSubmitHandler = (values: any) => {
    setLoading(true);
    register(values)
      .then(() => {
        setErrorMessage(null);
        router.push(`/verify?username=${values?.username}`);
        // reset({
        //   username: "",
        //   email: "",
        //   password: "",
        // });
      })
      .catch((err) => {
        setErrorMessage(err?.message);
      })
      .finally(() => {
        // setLoading(false);
      });
  };

  return (
    <>
      <Head>
        <title>Register</title>
      </Head>
      <Container component={"main"} maxWidth="xs">
        <FormProvider {...methods}>
          <Box
            component="form"
            noValidate
            autoComplete="off"
            autoCorrect="off"
            onSubmit={handleSubmit(onSubmitHandler)}
            sx={{
              background: "#FFF",
              marginTop: 8,
              display: "flex",
              flexDirection: "column",
              alignItems: "stretch",
              borderRadius: 3,
              padding: 3,
              boxShadow: 1,
            }}
          >
            <Stack width={"100%"}>
              <Typography variant="h2" marginBottom={4}>
                Register
              </Typography>
              {errorMessage && <Alert severity="error">{errorMessage}</Alert>}
              <Stack gap={2} mt={errorMessage ? 2 : 0}>
                <InputText
                  disabled={loading}
                  name="username"
                  required={true}
                  fullWidth={true}
                  label={"Username"}
                  set={setValue}
                />
                <InputText
                  disabled={loading}
                  name="email"
                  required={true}
                  fullWidth={true}
                  label={"Email"}
                  set={setValue}
                />
                <InputText
                  disabled={loading}
                  name="password"
                  required={true}
                  fullWidth={true}
                  label={"Password"}
                  additionalRegex={false}
                  set={setValue}
                />
              </Stack>
              <Button type="submit" fullWidth sx={{ mt: 3 }} disabled={loading}>
                {loading ? (
                  <CircularProgress size={22} />
                ) : (
                  <Typography variant="body" fontWeight={700}>
                    Register
                  </Typography>
                )}
              </Button>
              <Stack
                direction={"row"}
                alignItems={"center"}
                justifyContent={"center"}
                mt={4}
              >
                <Typography variant="button">
                  {"Already have an account? "}
                </Typography>
                <Button onClick={() => router.push("/login")} variant="text">
                  Login
                </Button>
              </Stack>
            </Stack>
          </Box>
        </FormProvider>
      </Container>
    </>
  );
};

export default Register;
