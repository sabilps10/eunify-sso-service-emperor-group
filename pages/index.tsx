import useLocalStorage from "@hooks/useLocalStorage";
import { Box, Button, Container } from "@mui/material";
import { NextPage } from "next";
import Head from "next/head";
import { useRouter } from "next/router";
import React, { useEffect } from "react";

const IndexPage: NextPage = () => {
  const router = useRouter();
  const { getItem } = useLocalStorage();

  useEffect(() => {
    if (!getItem("account")) {
      router.replace("/login");
    } else {
      router.replace("/home");
    }
  }, []);

  return (
    <>
      {/* <Head>
        <title>Amazon Auth Test</title>
      </Head>
      <Box display="flex" flex={1} justifyContent={"center"} mt={20}>
        <Button
          sx={{
            paddingX: 8,
          }}
          onClick={() => router.push("/login")}
        >
          Login
        </Button>
      </Box> */}
    </>
  );
};

export default IndexPage;
