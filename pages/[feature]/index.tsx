import ForgotPassword from "@features/authentication/ForgotPassword";
import Login from "@features/authentication/Login";
import Register from "@features/authentication/Register";
import Verify from "@features/authentication/Verify";
import Home from "@features/home";
import { NextPageWithProvider } from "@pages/_app";
import { getFeatureSlug } from "@utils";
import { GetServerSideProps } from "next";

const Feature: NextPageWithProvider<{ feature: string }> = ({ feature }) => {
  const getFeatureComponent = () => {
    switch (feature) {
      case "login":
        return <Login />;
      case "register":
        return <Register />;
      case "forgot-password":
        return <ForgotPassword />;
      case "home":
        return <Home />;
      case "verify":
        return <Verify />;

      default:
        return <div />;
    }
  };

  return <>{getFeatureComponent()}</>;
};

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const featureSlug = ctx.params?.feature as string;
  let feature = getFeatureSlug(featureSlug);

  if (!feature) {
    return {
      notFound: true, //redirects to 404 page
    };
  }

  return {
    props: {
      feature,
    },
  };
};

export default Feature;
