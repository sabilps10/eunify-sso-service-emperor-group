import Head from "next/head";

import { NextPageWithProvider } from "@pages/_app";
import { Typography, Box } from "@mui/material";

const DashboardPage: NextPageWithProvider = () => {
  return (
    <>
      <Head>
        <title>Not Found</title>
      </Head>
      <Box
        display="flex"
        marginTop={20}
        flexDirection="column"
        alignItems="center"
        width="100%"
        height="100%"
      >
        <Typography variant="h4" fontWeight={700}>
          404 - Page Not Found
        </Typography>
      </Box>
    </>
  );
};

export default DashboardPage;
