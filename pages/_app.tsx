import type { NextPage } from "next";
import type { AppProps } from "next/app";
import { useState } from "react";

import MuiThemeProvider from "@providers/MuiThemeProvider";
import ReactQueryProvider from "@providers/ReactQueryProvider";
import BaseLayout from "@components/Layouts/BaseLayout";

export type NextPageWithProvider<Props = {}> = NextPage<Props>;

type AppPropsWithLayout = AppProps & {
  Component: NextPageWithProvider;
};

function AuthTestingApp({ Component, pageProps }: AppPropsWithLayout) {
  const [status] = useState<boolean>(false);

  return (
    <MuiThemeProvider>
      <ReactQueryProvider>
        {typeof window !== "undefined" && (
          <BaseLayout>
            <Component {...pageProps} />
          </BaseLayout>
        )}
      </ReactQueryProvider>
    </MuiThemeProvider>
  );
}

export default AuthTestingApp;
