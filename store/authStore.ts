import { create } from "zustand";
import { persist } from "zustand/middleware";

type AuthState = {
  data: any;
};

type AuthAction = {
  setData: (data: any) => void;
};
export type AuthStoreTypes = AuthState & AuthAction;

const authStore = create(
  persist<AuthStoreTypes>(
    (set) => ({
      data: {},
      setData: async (data: any) => {
        set(() => ({
          data,
        }));
      },
    }),
    { name: "@user_data" }
  )
);

export default authStore;
