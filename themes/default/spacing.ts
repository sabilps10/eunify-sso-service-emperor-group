import { createSpacing, createTheme } from "@mui/system";

const spacingTheme = createTheme({ spacing: createSpacing() });

export const { spacing } = spacingTheme;

export default spacingTheme;
