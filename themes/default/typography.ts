import { createTheme } from "@mui/material";
import { TypographyOptions } from "@mui/material/styles/createTypography";

import colorsTheme from "./colors";

declare module "@mui/material/styles/" {
  interface TypographyVariants {
    h1: React.CSSProperties;
    h2: React.CSSProperties;
    h3: React.CSSProperties;
    h4: React.CSSProperties;
    h5: React.CSSProperties;
    h6: React.CSSProperties;
    h7?: React.CSSProperties;
    bigTextB: React.CSSProperties;
    note: React.CSSProperties;
    noteB: React.CSSProperties;
    smallNote: React.CSSProperties;
    tinyNote: React.CSSProperties;
    note120: React.CSSProperties;
    body: React.CSSProperties;
    bodyTextB: React.CSSProperties;
    mobileMenu: React.CSSProperties;
    formLabel: React.CSSProperties;
    formPlaceholder: React.CSSProperties;
  }

  interface TypographyVariantsOptions {
    h1?: React.CSSProperties;
    h2?: React.CSSProperties;
    h3?: React.CSSProperties;
    h4?: React.CSSProperties;
    h5?: React.CSSProperties;
    h6?: React.CSSProperties;
    h7?: React.CSSProperties;
    bigTextB?: React.CSSProperties;
    note?: React.CSSProperties;
    noteB?: React.CSSProperties;
    smallNote?: React.CSSProperties;
    tinyNote?: React.CSSProperties;
    note120?: React.CSSProperties;
    body?: React.CSSProperties;
    bodyTextB?: React.CSSProperties;
    mobileMenu?: React.CSSProperties;
    formLabel?: React.CSSProperties;
    formPlaceholder?: React.CSSProperties;
  }
}

declare module "@mui/material/Typography" {
  interface TypographyPropsVariantOverrides {
    h1: true;
    h2: true;
    h3: true;
    h4: true;
    h5: true;
    h6: true;
    h7: true;
    bigTextB: true;
    note: true;
    noteB: true;
    smallNote: true;
    tinyNote: true;
    note120: true;
    body: true;
    bodyTextB: true;
    mobileMenu: true;
    formLabel: true;
    formPlaceholder: true;
  }

  type Variant = keyof TypographyPropsVariantOverrides;
}

const newTypography: TypographyOptions = {
  allVariants: {
    fontFamily: "Montserrat",
    color: colorsTheme.palette.neutrals.text
  },
  h1: {
    fontFamily: "Montserrat",
    fontSize: "40px",
    lineHeight: "56px",
    letterSpacing: "-0.02em"
  },
  h2: {
    fontFamily: "Montserrat",
    fontWeight: 300,
    fontSize: "28px"
  },
  h3: {
    fontFamily: "Montserrat",
    fontWeight: 700,
    fontSize: "20px"
  },
  h4: {
    fontFamily: "Montserrat",
    fontWeight: 600,
    fontSize: "16px"
  },
  h5: {},
  h6: {
    fontSize: "24px",
    lineHeight: "32px",
    letterSpacing: "-0.01em;"
  },
  h7: {},
  bigTextB: {
    fontFamily: "Montserrat",
    fontWeight: 600,
    fontSize: "14px"
  },
  note: {
    fontFamily: "Montserrat",
    fontWeight: 500,
    fontSize: "12px"
  },
  noteB: {
    fontFamily: "Montserrat",
    fontWeight: 600,
    fontSize: "12px"
  },
  smallNote: {
    fontFamily: "Montserrat",
    fontWeight: 500,
    fontSize: "11px"
  },
  tinyNote: {
    fontFamily: "Montserrat",
    fontWeight: 500,
    fontSize: "10px"
  },
  note120: {
    fontFamily: "Montserrat",
    fontWeight: 400,
    fontSize: "12px",
    lineHeight: "14.4px"
  },
  body: {
    fontFamily: "Montserrat",
    fontWeight: 400,
    fontSize: "13px",
    lineHeight: "20.8px"
  },
  bodyTextB: {
    fontFamily: "Montserrat",
    fontWeight: 600,
    fontSize: "13px",
    lineHeight: "22.4px"
  },
  mobileMenu: {
    fontFamily: "Montserrat",
    fontWeight: 500,
    fontSize: "10px"
  },
  formLabel: {
    fontFamily: "Montserrat",
    fontWeight: 400,
    fontSize: "13px"
  },
  formPlaceholder: {
    fontFamily: "Montserrat",
    fontWeight: 400,
    fontSize: "16px"
  }
};

const typographyTheme = createTheme({
  typography: newTypography
});

export const { typography } = typographyTheme;

export default typographyTheme;
