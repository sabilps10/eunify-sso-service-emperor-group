import { Components, alpha } from "@mui/material";
import { radioClasses } from "@mui/material/Radio";

import { palette } from "@themes/default/colors";
import { typography } from "@themes/default/typography";

const MuiRadio: Components["MuiRadio"] = {
  styleOverrides: {
    colorPrimary: {
      color: palette.brandBlue[500]
    },
    root: {
      [`&.${radioClasses.checked}`]: {
        color: palette.brandBlue[500]
      }
    }
  }
};

export default MuiRadio;
