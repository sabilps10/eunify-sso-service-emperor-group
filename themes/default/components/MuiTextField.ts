import { Components, alpha } from "@mui/material";
import { outlinedInputClasses } from "@mui/material/OutlinedInput";

import { palette } from "@themes/default/colors";
import { typography } from "@themes/default/typography";

const MuiTextField: Components["MuiTextField"] = {
  styleOverrides: {
    root: {
      borderRadius: "5px",
      background: palette.brandGrey[500],

      fieldset: {
        border: "none"
      },

      input: {
        ...typography.bigTextB
      },

      [`.${outlinedInputClasses.focused}`]: {
        fieldset: {
          border: "none"
        }
      }
    }
  }
};

export default MuiTextField;
