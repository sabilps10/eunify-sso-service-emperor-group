import { Components, alpha } from "@mui/material";

import { spacing } from "@themes/default/spacing";
import { typography } from "@themes/default/typography";

export const MuiFormLabel: Components["MuiFormLabel"] = {
  styleOverrides: {
    root: {
      ...typography.h4,
      marginBottom: spacing(0.4),
      "&.Mui-focused": {
        color: "hsla(0, 0%, 27%, 1)"
      }
    }
  }
};
