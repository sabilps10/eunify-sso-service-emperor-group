import type { FC, PropsWithChildren } from "react";
import { ThemeProvider, CssBaseline } from "@mui/material";

import defaultTheme from "@themes/default";

const MuiThemeProvider: FC<PropsWithChildren> = ({ children }) => (
  <ThemeProvider theme={defaultTheme}>
    <CssBaseline />
    {children}
  </ThemeProvider>
);

export default MuiThemeProvider;
