type ErrorCognito = {
  code: string;
  name: string;
  message: string;
};
