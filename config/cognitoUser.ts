import useLocalStorage from "@hooks/useLocalStorage";
import userPool from "@userPool";
import {
  CognitoAccessToken,
  CognitoIdToken,
  CognitoRefreshToken,
  CognitoUser,
  CognitoUserSession,
  CookieStorage,
} from "amazon-cognito-identity-js";
import { LOCALSTORAGE_KEY } from "./constants";

const cognitoUser = (username?: string): CognitoUser => {
  const { getItem } = useLocalStorage();
  const cognitoUserVar = new CognitoUser({
    Username: username || getItem(LOCALSTORAGE_KEY.ACCOUNT) || "",
    Pool: userPool,
    Storage: new CookieStorage({
      domain: process.env.NEXT_PUBLIC_DOMAIN
        ? process.env.NEXT_PUBLIC_DOMAIN
        : "http://localhost:3000",
      secure: true,
    }),
  });

  const session = new CognitoUserSession({
    AccessToken: new CognitoAccessToken({
      AccessToken: getItem(LOCALSTORAGE_KEY.ACCESS_TOKEN) || "",
    }),
    IdToken: new CognitoIdToken({
      IdToken: getItem(LOCALSTORAGE_KEY.ID_TOKEN) || "",
    }),
    RefreshToken: new CognitoRefreshToken({
      RefreshToken: getItem(LOCALSTORAGE_KEY.REFRESH_TOKEN) || "",
    }),
  });


  if (session.isValid()) {
    cognitoUserVar.setSignInUserSession(session);
  }

  return cognitoUserVar;
};

export default cognitoUser;
