export const LOCALSTORAGE_KEY = {
  ACCESS_TOKEN: "access_token",
  ID_TOKEN: "id_token",
  REFRESH_TOKEN: "refresh_token",
  ACCOUNT: "account",
  EXPIRED_TOKEN: "expired_token",
  EXPIRED_TIME: "expired_time",
};
export const NON_LOGIN_ROUTES_KEY = [
  "login",
  "register",
  "forgot-password",
  "verify",
];

export const ROUTES_KEY = ["home"];
