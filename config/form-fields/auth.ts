import { object, string } from "zod";

export const loginSchema = object({
  username: string().min(1, { message: "Username is required" }),
  password: string().min(1, { message: "Password is required" }),
});

export const mfaSchema = object({
  totp: string().min(1, { message: "TOTP Code is required" }),
});

export const forgotPasswordSchema = object({
  username: string().min(1, { message: "Username is required" }),
});

export const changePasswordSchema = object({
  otp: string().min(1, { message: "Otp is required" }),
  new_password: string().min(1, { message: "New Password is required" }),
});

export const registerSchema = object({
  username: string().min(1, { message: "Username is required" }),
  email: string().min(1, { message: "Email is required" }).email(),
  password: string().min(1, { message: "Password is required" }),
});

export const verifySchema = object({
  otp: string().min(1, { message: "OTP code is required" }),
});
