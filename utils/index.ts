import { NON_LOGIN_ROUTES_KEY, ROUTES_KEY } from "@config/constants";

export const getFeatureSlug = (feature: string) => {
  if (
    !ROUTES_KEY.includes(feature) &&
    !NON_LOGIN_ROUTES_KEY.includes(feature)
  ) {
    return null;
  } else {
    return feature;
  }
};
